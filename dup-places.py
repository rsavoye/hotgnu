#!/usr/bin/python3

# 
# Copyright (C) 2020   Free Software Foundation, Inc.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#

# standard modules
import os
import sys
import logging
import re
import json
import getopt
from sys import argv
import psycopg2
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from shapely import wkt, wkb
import shapely.geometry
from shapely.geometry import Point
# PieSpinner, MoonSpinner, LineSpinner also available
from progress.spinner import PixelSpinner


def usage(argv):
    out = """
    help(-h)     Get command line options
    verbose(-v)  Enable verbose output
    database(-d) Database to use
    school(-s)   Distance between school and place with the same name
    places(-p)   Distance between places with the same name
    """
    print(out)
    quit()

if len(argv) <= 1:
    usage(argv)

try:
    (opts, val) = getopt.getopt(argv[1:], "h,v,d:,p:,s:",
        ["help", "verbose", "database", "places", "schools"])
except getopt.GetoptError as e:
    logging.error('%r' % e)
    usage(argv)
    quit()

# default values for command line options
options = dict()
options['database'] = "Cusco"
options['schooldist'] = 0.2
options['placedist'] = 0.9
 
for (opt, val) in opts:
    if opt == '--help' or opt == '-h':
        usage(argv)
    elif opt == "--database" or opt == '-d':
        options['database'] = val
    elif opt == "--school" or opt == '-s':
        options['schooldist'] = float(val)
    elif opt == "--place" or opt == '-p':
        options['placedist'] = float(val)


class OsmFile(object):
    """OSM File output"""
    def __init__(self, filespec=None):
        if filespec:
            self.file = open(filespec, 'w')
            self.file.write('<?xml version=\'1.0\' encoding=\'UTF-8\'?>\n')
            self.file.write('<osm version="0.6" generator="hotgnu 0.1">\n')
    def footer(self):
        self.file.write("</osm>\n")
        if self.file != False:
            self.file.close()

    def writeNode(self, node):
        self.file.write("<node id='%s' lat='%f' lon='%f' action='modify' version='2' >" % (node['osm_way_id'], node['wkb'].y, node['wkb'].x))
        for key,value in node.items():
            if key == 'osm_way_id' or key == 'lat' or key == 'lon'  or key == 'wkb':
                continue
            else:
                self.file.write("\n  <tag k='%s' v='%s'/>" % (key, value))
        self.file.write("\n</node>\n")

# Make an array out of the string in other_tags, which is
# a simple 'key' => 'value'.
def makeArray(other_tags):
    result = dict()
    for tag in other_tags.split('\", '):
        var = tag.replace('"', '').split('=>')
        result[var[0]] = var[1].replace("'", '')
    return result

# parse the line from the SQL query. Note this means the same
# fields must be in the same position in the query.
def parseLine(line):
    #print("LINE: %r" % line)
    result = dict()
    result['osm_way_id'] = line[0]
    if line[1]:
        result['name'] = line[1].replace("'", '')
    else:
        result['name'] = line[1]        
    result['place'] = line[2]
    result['wkb'] =  wkb.loads(line[4], hex=True)[0]
    if len(line) > 5:
        result['dist'] = round(float(line[5]), 4)
    # process other_tags if they exist
    if line[3]:
        tmp = result.copy()
        tmp.update(makeArray(line[3]))
        return tmp
    else:
        return result


db = options['database']
connect = "dbname='" + db + "'"
dbshell = psycopg2.connect(connect)
dbshell.autocommit = True
dbcursor = dbshell.cursor()

# Get all the schools as the base for the validation
query = "SELECT osm_id,name,place,other_tags,wkb_geometry AS wkb FROM points WHERE other_tags::hstore->'amenity'='school'"
dbcursor.execute(query)
schools = list()
line = dbcursor.fetchone()
while line is not None:
    schools.append(parseLine(line))
    line = dbcursor.fetchone()


#
# Find all the places near a school
#
dist = options['schooldist']
osmmissing = OsmFile("places-missing.osm")
osm = OsmFile("places-schools.osm")
bar = PixelSpinner('Processing places near schools in: %s...' % db)
for school in schools:
    places = list()
    bar.next()
    if 'addr:subdistrict' not in school:
        continue

    if 'addr:full' not in school:
        school['addr:full'] = ""
    query = """SELECT osm_id,name,place,other_tags,wkb_geometry as wkb,St_Distance(wkb_geometry,'SRID=4326;%s') AS dist FROM points WHERE St_Distance(wkb_geometry, 'SRID=4326;%s')<%f AND osm_id!='%s' AND place is not NULL AND (levenshtein(name, \'%s\')=0 OR levenshtein(name, \'%s\')=0 OR levenshtein(name, \'%s\')=0) ORDER BY dist""" % (school['wkb'].wkb_hex, school['wkb'].wkb_hex, dist, school['osm_way_id'], school['addr:subdistrict'].replace("'", ''), school['addr:subdistrict'].replace("'", ''), school['addr:full'].replace("'", ''))
    dbcursor.execute(query)
    line = dbcursor.fetchone()
    while line is not None:
        places.append(parseLine(line))
        line = dbcursor.fetchone()
    if len(places) == 0:
        #print("%s MISSING! %s" % (school['addr:subdistrict'], query))
        school['fixme'] = "School as no associated place!"
        osmmissing.writeNode(school)
        continue
    matched = 0
    for place in places:
        if 'name' in place:
            # tmp = [school['addr:district'], school['addr:subdistrict'], school['addr:province']]
            tmp = [school['addr:district'], school['addr:full'], school['addr:subdistrict']]
            # drop duplicate fields
            fields = list(dict.fromkeys(tmp))
            match = fuzz.token_sort_ratio(school['addr:subdistrict'], place['name'])
            # print("Compared(%d) %s vs %s" % (match, school['addr:subdistrict'], place['name']))
            if match >= 80:
                matched += 1
                # print("MATCHED: %d: %r == %r" % (matched, school['addr:subdistrict'], place['name']))
                osm.writeNode(school)
                # osm.writeNode(place)
    if matched == 0:
        match = fuzz.token_sort_ratio(school['addr:full'], place['name'])
        # print("Compared 2 (%d) %s vs %s" % (match, school['addr:full'], place['name']))
        if match < 80:
     # print("Nothing found for %s" % school['addr:subdistrict'])
            school['fixme'] = "School name does not match any place!"
            osmmissing.writeNode(school)
    if matched > 1:
        #print("FIXME: %f, %d, %s" % (school['wkb'].distance(place['wkb']), matched, school['addr:subdistrict']))
        for place in places:
            place['fixme'] = "Duplicate place found near school!"
            osm.writeNode(place)

bar.finish()
osmmissing.footer()
osm.footer()

#
# Find all the duplicate places
#
places = list()
query = """SELECT osm_id,name,place,other_tags,wkb_geometry AS wkb FROM points WHERE place is not NULL ORDER BY name;"""
dbcursor.execute(query)
line = dbcursor.fetchone()
while line is not None:
    places.append(parseLine(line))
    line = dbcursor.fetchone()

dist = options['placedist']
osm = OsmFile("places-dups.osm")
bar = PixelSpinner('Processing duplicate places in: %s...' % db)
for place in places:
    bar.next()
    if 'name' not in place or place['name'] is None:
        continue
    else:
        name = place['name'].replace("'", '')
        query = """SELECT osm_id,name,place,other_tags,wkb_geometry as wkb,St_Distance(wkb_geometry,'SRID=4326;%s') AS dist FROM points WHERE St_Distance(wkb_geometry, 'SRID=4326;%s')<%f AND osm_id!='%s' AND place is not NULL AND name is not NULL AND levenshtein(name, \'%s\')=0 ORDER BY dist""" % (place['wkb'].wkb_hex, place['wkb'].wkb_hex, dist, place['osm_way_id'], name)

    dbcursor.execute(query)
    line = dbcursor.fetchone()
    dups = list()
    while line is not None:
        dups.append(parseLine(line))
        line = dbcursor.fetchone()
    if len(dups) > 1:
        for dup in dups:
            dup['fixme'] = "Possible duplicate place"
            osm.writeNode(dup)

osm.footer()
bar.finish()

#
# Get a list of places that don't have a highway nearby
#
if False:                        # optional, takes a long time
    count = 0
    osm = None
    dist = options['placedist']
    bar = PixelSpinner('Processing roads near places in: %s...' % db)
    for place in places:
        bar.next()
        query = """SELECT osm_id,name,other_tags,wkb_geometry as wkb,St_Distance(wkb_geometry,'SRID=4326;%s') AS dist FROM lines WHERE St_Distance(wkb_geometry, 'SRID=4326;%s')<%f AND osm_id!='%s' AND highway is not NULL AND highway!='path' ORDER BY dist LIMIT 1""" % (place['wkb'].wkb_hex, place['wkb'].wkb_hex, dist, place['osm_way_id'])
        dbcursor.execute(query)
        line = dbcursor.fetchone()
        if line is None:
            place['fixme'] = "Place needs an access road!"
            osm.writeNode(place)
            count += 1
            if osm is None:
                osm = OsmFile("places-roads.osm")
    if osm:
        osm.footer()
    bar.finish()
    if count == 0:
        print("All places have road access")
