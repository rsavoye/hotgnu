# HotGNU

A collection of python scripts for helping with bulk validation of
OSM map data.

dup-places.py - Find duplicate places.

examples:
./dup-places.py -d Cusco -h

    help(-h)     Get command line options
    verbose(-v)  Enable verbose output
    database(-d) Database to use (default Cusco)
    school(-s)   Distance between school and place with the same name
    places(-p)   Distance between places with the same name

All it needs to work is a postgres database with the fuzzystrmatch,
postgis, hstore extension loaded after creating the database. Rather
than use osm2pgsql, I use ogr, so import into postgres like this: 

ogr2ogr -skipfailures -progress -overwrite -f  "PostgreSQL" PG:"dbname=${dbname}" -nlt GEOMETRYCOLLECTION ${infile} -lco COLUMN_TYPES="other_tags=hstore"

To populate the database, I download the data from Geofabrik, then use
an osmosis poly file and osmconvert to extract just the one state you
want to validate. 

